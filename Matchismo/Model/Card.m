//
//  Card.m
//  Matchismo
//
//  Created by Mckein on 11/28/13.
//  Copyright (c) 2013 Mckein. All rights reserved.
//

#import "Card.h"

@interface Card()

@end

@implementation Card

//This method already exist
@synthesize contents = _contents; //instance variable with the name _contents
@synthesize chosen = _chosen;
@synthesize matched = _matched;

//This method already exist
-(NSString *)contents{ //getter method
    return _contents;
}

-(void)setContents:(NSString *)contents{ //setter
    _contents = contents;
}

-(BOOL)isChosen{
    return _chosen;
}

-(void)setChosen:(BOOL)chosen{
    _chosen = chosen;
}

-(BOOL)isMatched{
    return _matched;
}

-(void)setMatched:(BOOL)matched{
    _matched = matched;
}

-(int)match:(NSArray *)otherCards{
    //all local variables is set to zero;
    int score = 0;
    
    //we only used the dot notation for properties
    //== only compares instance
    
    for (Card *card in otherCards){
        if ([card.contents isEqualToString:self.contents]){
            score = 1;
        }
    }
    
    return score;
}

@end
